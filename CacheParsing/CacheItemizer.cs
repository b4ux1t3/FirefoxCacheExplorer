
namespace CacheParsing;

public static class CacheItemizer
{
    public static IReadOnlyDictionary<int, List<CacheItem>> GroupByAppId(IEnumerable<CacheItem> items) =>
        items
            .GroupBy(i => i.AppId)
            .Select(g => 
                (
                    appId: g.Key, 
                    cacheItems: g.Select(i => i).ToList())
                )
            .ToDictionary(t => t.appId)
            .Select(kvp =>
                new KeyValuePair<int, List<CacheItem>>(kvp.Key, kvp.Value.cacheItems))
            .ToDictionary();

}