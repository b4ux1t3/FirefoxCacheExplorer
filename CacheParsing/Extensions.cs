namespace CacheParsing;

public static class Extensions
{
    public static uint ReadFileSize(this BinaryReader reader)
    {
        var bytes = reader.ReadBytes(3);
        var number = (uint)(0 | (bytes[0] << (8 * 2)) | (bytes[1] << (8 * 1)) | bytes[2]);

        return number;
    }

    public static bool IsJpg(string filePath)
    {
        var bytes = File.ReadAllBytes(filePath);
        return bytes.Length > 10 && bytes[0] == 0xFF && bytes[1] == 0xD8 && bytes[2] == 0xFF && bytes[3] == 0xE0 &&
               bytes[6] == 0x4A && bytes[7] == 0x46 && bytes[8] == 0x49 && bytes[9] == 0x46 && bytes[10] == 0x00;
    }
}