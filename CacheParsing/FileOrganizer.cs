using System.Text.RegularExpressions;

namespace CacheParsing;

public static partial class FileOrganizer
{
    public static void OrganizeFilesByAppId(string cacheDirectory, string outputLocation,
        IReadOnlyDictionary<int, List<CacheItem>> dictionary)
    {
        if (!Directory.Exists(cacheDirectory))
            throw new ArgumentException($"Cache directory does not exist: {cacheDirectory}");

        if (!CanStoreFiles(outputLocation))
            throw new ArgumentException($"Cannot store files in: {outputLocation}");

        var cacheItemDirectory = $"{cacheDirectory}/entries";

        foreach (var (key, items) in dictionary)
        {
            var targetDirectory = $"{outputLocation}/{key:X}";
            if (!CanStoreFiles(targetDirectory)) continue;

            // Parallel.ForEach(items, (Action<CacheItem>)CopyFile);
            foreach (var cacheItem in items)
            {
                CopyFile(cacheItem);
            }

            continue;

            void CopyFile(CacheItem item)
            {
                // If there isn't a file to check, we ignore this item.
                var cacheEntry = $"{cacheItemDirectory}/{item.Filename}";
                if (!File.Exists(cacheEntry)) return;

                // If we can't store things in a subdirectory, we ignore this item
                var targetFileTypeSubdirectory = $"{targetDirectory}/{ContentTypeToDirectory(item.ContentType)}";
                if (!CanStoreFiles(targetFileTypeSubdirectory)) return;

                // If the entry can't be parsed, we ignore this item.
                ParsedCacheItem? parsedEntry = null;
                try
                {
                    parsedEntry = CacheEntryParser.ParseCacheItem(File.ReadAllBytes(cacheEntry));
                }
                catch
                {
                    return;
                }

                if (parsedEntry is null) return;

                var filename = parsedEntry.OriginalFileName(item.Filename);

                // Now we know we have a file, we know the file has metadata. . .
                var metadataFileContents = parsedEntry.MetadataString;

                var destFileName = _getFileName(parsedEntry, filename);
                var cacheEntryDirectory = $"{targetFileTypeSubdirectory}/{destFileName}";
                
                // If we can't make a directory to hold the contents and metadata of. . .well you get the idea
                if (!CanStoreFiles(cacheEntryDirectory)) return;
                
                var destFilePath = $"{cacheEntryDirectory}/{destFileName}";

                File.WriteAllBytes(destFilePath, parsedEntry.FileContents);
                File.WriteAllText($"{cacheEntryDirectory}/metadata", metadataFileContents);
            }
        }
    }

    private static ReadOnlySpan<char> _getFileName(ParsedCacheItem parsedEntry, string filename)
    {
        var destFileName = parsedEntry.OriginalFileName(filename);
        if (parsedEntry.Lookup.TryGetValue("content-type", out var imageContentType) && imageContentType.Contains("image/"))
        {
            if (parsedEntry.Lookup.TryGetValue("content-disposition", out var contDisp) &&
                ContentDispositionRegex().IsMatch(contDisp))
            {
                return ContentDispositionRegex().Match(contDisp).Groups[1].Value;
            }
        }
        if (!FileExtensionRegex().IsMatch(destFileName) && parsedEntry.Lookup.TryGetValue("Content-Type", out var contentType))
        {
            destFileName += _getExtensionByContentType(contentType);
        }

        return destFileName;
    }

    private static string _getExtensionByContentType(string contentType) =>
        contentType switch
        {
            "image/jpeg" => ".jpg",
            "image/gif" => ".gif",
            "image/png" => ".png",
            "image/svg+xml" => ".svg",
            "image/webp" => ".webp",
            _ => ""
        };

    private static bool CanStoreFiles(string directory)
    {
        if (Directory.Exists(directory)) return true;

        try
        {
            Directory.CreateDirectory(directory);
        }
        catch
        {
            return false;
        }

        return Directory.Exists(directory);
    }

    private static string ContentTypeToDirectory(ContentType type) => type switch
    {
        ContentType.Unknown => "Unknown",
        ContentType.Other => "Other",
        ContentType.Javascript => "Javascript",
        ContentType.Image => "Image",
        ContentType.Media => "Media",
        ContentType.Stylesheet => "Stylesheet",
        ContentType.Wasm => "Wasm",
        ContentType.Last => "Last",
        _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
    };
    [GeneratedRegex(@".*\.\w+")]
    private static partial Regex FileExtensionRegex();

    [GeneratedRegex(@"[^']*'+([_\w]*\.\w+)")]
    private static partial Regex ContentDispositionRegex();
}