namespace CacheParsing;

public record IndexMetadata(int Version, int LastWritten, int Dirty);