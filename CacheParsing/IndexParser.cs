namespace CacheParsing;

public static class IndexParser
{
    private const int EntryLength = 0x28;
    private const int HashLength = 20;
    
    public static List<CacheItem> ParseCacheIndex(string cacheLocation)
    {
        if (!Directory.Exists(cacheLocation)) 
            throw new InvalidOperationException($"The directory ${cacheLocation} does not exist.");
        var indexFile = $"{cacheLocation}/index";

        return ParseCacheIndex(File.OpenRead(indexFile));
    }

    public static List<CacheItem> ParseCacheIndex(Stream fileStream)
    {
        var fr = new BinaryReader(fileStream);
        
        List<CacheItem> cacheItems = [];


        var version = fr.ReadInt32();
        var lastWritten = fr.ReadInt32();
        var dirty = fr.ReadInt32();

        var metadata = new IndexMetadata(version, lastWritten, dirty);
        fr.BaseStream.Seek(0x10, SeekOrigin.Begin);

        while (fr.BaseStream.Length - fr.BaseStream.Position > 36)
        {
            cacheItems.Add(_readCacheItem(fr));
        }

        return cacheItems;
    }
    
    private static CacheItem _readCacheItem(BinaryReader reader)
    {
        var bytes = reader.ReadBytes(41);

        return new(bytes);
    }
}