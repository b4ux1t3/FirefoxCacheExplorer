using System.Globalization;
using System.Text;

namespace CacheParsing;

public record CacheItemMetadata(string Header, string Value)
{
    public override string ToString() => $"{Header}= {Value}";
}

public record ParsedCacheItem(byte[] FileContents, CacheItemMetadata[] Metadata)
{
    public string MetadataString => Metadata.Aggregate("", (state, metadata) => $"{state}\n{metadata}").Trim();
    public Dictionary<string, string> Lookup { get; } = Metadata.Select(m => (m.Header, m.Value)).DistinctBy(m => m.Header).ToDictionary();

    /// <summary>
    /// Gets the filename of the original file from the metadata.
    /// </summary>
    /// <param name="defaultFilename">The default value we want to use if we cannot locate a filename.</param>
    /// <returns></returns>
    public string OriginalFileName(string defaultFilename)
    {
        var returnValue = 
            Lookup.TryGetValue("url", out var output)
                ? output.Split('?')[0].Split('/')[^1]
                : defaultFilename;
        return string.IsNullOrEmpty(returnValue) || returnValue.Length > 30
            ? defaultFilename
            : returnValue;
    }
}

public class CacheParsingException(string msg) : Exception(msg);

public static class CacheEntryParser
{
    private static ParsedCacheItem _stripMetadata(
        ReadOnlySpan<byte> cacheItemBytes)
    {
        var bufferSize = 5;
        for (var i = cacheItemBytes.Length - bufferSize - 1; i > 0; i--)
        {
            if (cacheItemBytes[i] != ':' ||
                Encoding.ASCII.GetString(cacheItemBytes[i..(i + bufferSize)]) != ":http") continue;
            var (contents, metadataString) = (cacheItemBytes[..i].ToArray(), Encoding.ASCII.GetString(cacheItemBytes[i..]));

            var parsedMetadata = 
                metadataString
                    .Split('\n')
                    .Select(_splitOnFirstColon)
                    .ToArray();
            var parsedFirstLine = _interpretFirstHeaderItem(parsedMetadata[0]);

            CacheItemMetadata[] reformattedMetadata = [..parsedFirstLine, ..parsedMetadata[1..]];
            
            return new(contents, reformattedMetadata);
        }

        return new(cacheItemBytes.ToArray(), []);
    }

    public static ParsedCacheItem? ParseCacheItem(ReadOnlySpan<byte> cacheItemBytes) => _stripMetadata(cacheItemBytes);

    private static CacheItemMetadata _splitOnFirstColon(string line)
    {
        var split = line.Contains(':') ? line.Split(':', 2) : [Guid.NewGuid().ToString(), line];
        
        return new(split[0].Trim(), split[1].Trim());
    }

    private static List<CacheItemMetadata> _interpretFirstHeaderItem(CacheItemMetadata item)
    {
        if (!item.Value.StartsWith("https")) return [];
        List<CacheItemMetadata> items = [];
        var split = item.Value.Split('\0');
        
        for (var i = 0; i < split.Length; i++)
        {
            var str = split[i].Trim();
            if (str is "request-method" or "response-head") items.Add(new(str.ToLower(CultureInfo.InvariantCulture), split[++i].Trim()));
            if (str.StartsWith("http")) items.Add(new("url", str));
        }

        return items;
    }
    
    
}