namespace CacheParsing;

public enum ContentType
{
    Unknown = 0,
    Other = 1,
    Javascript = 2,
    Image = 3,
    Media = 4,
    Stylesheet = 5,
    Wasm = 6,
    Last = 7,
}

public record CacheItem(
    byte[] Bytes
)
{
    private const int HashLength = 20;
    private const int FrecencyOffset = HashLength;
    private const int ExpiryOffset = FrecencyOffset + 4;
    private const int AppIdOffset = ExpiryOffset + 4;
    private const int FlagsOffset = AppIdOffset + 4;
    private const int FileSizeOffset = FlagsOffset + 1;
    private const int ContentTypeOffset = FileSizeOffset + 3;

    private Span<byte> Hash => Bytes.AsSpan()[..HashLength];
    public int Frecency => _getIntFromBytes(Bytes[FrecencyOffset..(FrecencyOffset + 4)]);
    public int Expiry => _getIntFromBytes(Bytes[ExpiryOffset..(ExpiryOffset + 4)]);
    public int AppId => _getIntFromBytes(Bytes[AppIdOffset..(AppIdOffset + 4)]);
    public byte Flags => Bytes[FlagsOffset];
    public uint FileSize => _getUIntFromBytes([0,..Bytes[FileSizeOffset..(FileSizeOffset + 3)]]);
    public Span<byte> UnknownMetadata => Bytes.AsSpan()[(Bytes.Length - 4)..];

    public string Filename =>
        Hash
            .ToArray()
            .Select(b => b < 0x10 ? $"0{b:X}" : b.ToString("X"))
            .Aggregate((state, byt) => $"{state}{byt}");

    public ContentType ContentType => Bytes[ContentTypeOffset] switch
    {
        0 => ContentType.Unknown,
        1 => ContentType.Other,
        2 => ContentType.Javascript,
        3 => ContentType.Image,
        4 => ContentType.Media,
        5 => ContentType.Stylesheet,
        6 => ContentType.Wasm,
        7 => ContentType.Last,
        _ => throw new ArgumentOutOfRangeException($"{Bytes[ContentTypeOffset]:X} is not a valid content type"),
    };

    private static int _getIntFromBytes(byte[] bytes)
    {
        if (bytes.Length == 4) return bytes[0] << 24 | bytes[1] << 16 | bytes[2] << 8 | bytes[3];
        var howMany = bytes.Length < 4
            ? "too many"
            : "too few";
        throw new ArgumentException($"Byte has {howMany} bytes: {bytes.Length}");

    }

    private static uint _getUIntFromBytes(byte[] bytes)
    {
        if (bytes.Length == 4) return (uint) (bytes[0] << 24 | bytes[1] << 16 | bytes[2] << 8 | bytes[3]);
        var howMany = bytes.Length < 4
            ? "too many"
            : "too few";
        throw new ArgumentException($"Byte has {howMany} bytes: {bytes.Length}");
    }
}