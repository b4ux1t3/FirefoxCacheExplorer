﻿using CacheParsing;
using FileOrganizer = CacheParsing.FileOrganizer;

const int entryLength = 0x28;
const int hashLength = 20;
const int jumpAheadLength = entryLength - hashLength;

if (args.Any(arg => arg.Contains("-h")))
{
    Usage();
    return 0;
}

if (args.Length < 2)
{
    Console.Error.WriteLine("Must provide a cache location and output directory.");
    Usage();
    return 127;
}

var cacheLocation = args[0];
var outLocation = args[1];

if (!File.Exists($"{cacheLocation}/index"))
{
    Console.Error.WriteLine($"Index file does not exist: {cacheLocation}/index");
    return -255;
}

var cacheItems = IndexParser.ParseCacheIndex(cacheLocation);


Console.WriteLine($"Cache items: {cacheItems.Count}");


var groups = CacheItemizer.GroupByAppId(cacheItems);

FileOrganizer.OrganizeFilesByAppId(cacheLocation, outLocation, groups);


Console.WriteLine($"Application IDs: {groups.Count}");

var appsIWant = groups.Where(kvp => kvp.Value.Count > 600);

return 0;

void Usage()
{
    Console.WriteLine("usage: ff-cache-organizer FIREFOX_CACHE_LOCATION OUTPUT_DIRECTORY");
}