# Firefox Cache Explorer

This repository consists of a set of tools to explore, organize and archive the Firefox cache.

The primary purpose of this repo is to organize files by file type and origin. Right now, the only working tool is the `FirefoxCacheOrganizer`.

Point it to your Firefox cache (you'll have to look up its location, it varies) and an output directory, and it will parse the cache index and then sort cache entries into appropriate directories in your output directory.
