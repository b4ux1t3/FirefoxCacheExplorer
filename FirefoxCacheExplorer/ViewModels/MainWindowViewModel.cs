﻿using System.Collections.Generic;
using Avalonia.Platform.Storage;
using CacheParsing;

namespace FirefoxCacheExplorer.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
#pragma warning disable CA1822 // Mark members as static
    public string Greeting => "Welcome to Avalonia!";
#pragma warning restore CA1822 // Mark members as static

    public List<CacheItem> CacheItems = [];

    public IStorageFile? cacheFile;
}