using System.Collections.Generic;
using CacheParsing;

namespace FirefoxCacheExplorer;

public class CacheItemService
{
    public IReadOnlyDictionary<int, List<CacheItem>> CacheItemsByAppId { get; private set; } = new Dictionary<int, List<CacheItem>>();

    public string CachePath { get; private set; } = "";
    public void CreateDictionary(IEnumerable<CacheItem> items) => CacheItemsByAppId = CacheItemizer.GroupByAppId(items);
    public void SetCachePath(string path) => CachePath = string.IsNullOrEmpty(CachePath) ? path : CachePath;
}
