using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Platform.Storage;
using CacheParsing;
using FirefoxCacheExplorer.ViewModels;

namespace FirefoxCacheExplorer.Views;

public partial class MainWindow : Window
{
    private readonly CacheItemService _service;

    public MainWindow(CacheItemService service)
    {
        _service = service;

        InitializeComponent();
    }

    private async void Button_OnClick(object? sender, RoutedEventArgs e)
    {
        var files = await StorageProvider.OpenFilePickerAsync(new FilePickerOpenOptions()
        {
            Title = "Point to Firefox Cache",
            AllowMultiple = false,
        });

        var resolvedFile = files[0];
        var stream = await resolvedFile.OpenReadAsync();

        var indexFiles = IndexParser.ParseCacheIndex(stream);

        _service.CreateDictionary(indexFiles);

        ((MainWindowViewModel)DataContext).CacheItems = indexFiles;
    }
}